
# Proof of Treasure Coin (POTT)

Proof of Treasure Coin (POTT) is the token which rewards winners of proofoftreasure.com. This is a free to play, location finder game. This readme describes the token mechanics, and how 3rd parties can verify our code. 

1) How the Token works

The token runs on the chia blockchain, and uses the CAT2 standard. We have created a singleton coin which has a value of 285760001 mojos. This singleton uses a custom inner puzzle which can be found under 'Singleton_Inner_Puzzle.clsp'. This mints our Token every time it is spent, but can only be spent every 30 days. As the value of the singleton decreases, the each halving occurs at a specific point (see 1 for details).

The Token we have created is unique due to its TAIL (See chia CAT tokens for details). This TAIL checks it is created by our singleton when it is created, and in this way can only be minted by our singleton.


2) Overview of how our CAT (Chia Asset Token) is minted:

Our CAT includes some custom code in order to allow it to be minted over a defined maximum issuance schedule. The CAT has a TAIL which assertes that the CAT comes from a singleton coin with a defined puzzlehash. Only our singleton can have the defined puzzlehash, and no other legitamate coin (which could be spent) can have the same puzzlehash. The cat is then minted according to the rules in the singleton. The inner singleton code and CAT Tail code can be found in this repositry. 

3) Token IDs

The unique tail of our cat provides a unique asset ID. This is used to add the CAT to your wallet

    CAT ASSET ID: 5fd27391e6385e5d4bdc5b7b2df67b2e8698e337d49b94302a3551deda565e58
    
Our CAT is minted by a singleton which includes the code to assure the minting schedule. All singletons in chia are minted from a standard launcher puzzle.
    
    SINGLETON LAUNCHER ID: 0x6228aafc283bd3df64d9b34803b76b7d75ace0eab4975a2efd063b857f50fee1

The first singleton minted is the singleton which is used to mint our premine. This has a defined amount.
        
    SINGLETON EVE COIN ID: c651cd2096735befeb5f374d4f76d19df78bbfa53b29268911386af9cfa6467e
    SINGLTON EVE COIN AMOUNT: 285760001

Our Singleton contains a unique puzzlehash. If we change this puzzlehash the coin can no longer be spent, thereby ensuring the max mint schedule is adhered too. 

    SINGLETON PUZZLEHASH: 0x2f28cf6868baf7c7393d5b9f0210d3f9fc5a884276eea3b73be8c15ffc8b5476

4) Tokenomics 

The token can only be minted every 30 Days ( seconds). The amount which is minted decreases over time. In this way, the token is designed to have a maximum issuance schedule.  

The Issuance schedule is designed as follows: 

    First Mint (Premine)                                                   40000
    First Six Mints - amt per mint: (Minimum six months):                  8192 
    Second Six Mints - amt per mint: (Minimum six months):                 4096
    Third Six Mints - amt per mint: (Minimum six months):                  2048
    Fourth Six Mints - amt per mint: (Minimum six months):                 1024
    Final Minting value - amt per mint (continues for up to 25 years):    512

Three seperate wallets are used for our Game. The master Wallet, The developers Wallet, and the live wallet. 

    Master Wallet Adresses: xch170ednpm5g5fng2l39z8sq56qqksxgday2ncc00mjcvnj9xe8xgnsavmglv
    Developer Wallet Adresses: xch1srlv6htd6yj6hu8fnrmfk8qsdvhrg6u6s8umsw2pufsctylt8zqqwsu27h
    Live Wallet Adresses: xch1fvvu0az9lu0u55q60ercgns32zwyhg9fxpuumzr2rx7v3zte299q0wdp73

Because each Token is created from 1000 mojos, each mint within the chialisp has a value of 1000 times these amounts (In mojos)
All tokens will be minted directly into the Master wallet (xch170ednpm5g5fng2l39z8sq56qqksxgday2ncc00mjcvnj9xe8xgnsavmglv, xch1heptx67wm9r835ugdd07qk7frecxahzfrrrfrd0z7kznz78q2nwsnm357s). From there they will be distributed as follows.

    Premine minted from master wallet -> Developers Wallet (xch1srlv6htd6yj6hu8fnrmfk8qsdvhrg6u6s8umsw2pufsctylt8zqqwsu27h)
    Each mint from the master Wallet -> Gradually transferred to live wallet (xch1fvvu0az9lu0u55q60ercgns32zwyhg9fxpuumzr2rx7v3zte299q0wdp73) as required 

We (the developers) will use the premine in order to grow the game and increase the variety of games available as well as reward development. The wallet addresses, including Minting (Master), Developer and Game (Live) Wallet addresses, are available on our site for 3rd party monitoring.

5) Verifying our Token 

To verify the singleton exists and uses the code we specify the following details are useful:

    SINGLETON LAUNCHER ID: 0x6228aafc283bd3df64d9b34803b76b7d75ace0eab4975a2efd063b857f50fee1
    PUBKEY: a7a63a2a4f23d37ca729605e55d2d49260850be5bc45c7a0a4c993213138d16d4c9490712a117a645c3914c566ab509e
    SINGLETON PUZZLEHASH: 0x2f28cf6868baf7c7393d5b9f0210d3f9fc5a884276eea3b73be8c15ffc8b5476

You can verify our singleton exists on the chain according to the information given in this repositry. You can curry the relevant information into https://chialisp.com/docs/puzzles/singletons, and assert our coin works as specified.



*** A NOTE ON THE CHANGE FROM CAT1 TO CAT2 STANDARD ***

Chia updated from the CAT1 standard to the CAT2 standard on 26/07/22 due to a vulnerability with the CAT1 standard. Our game was recently released at this time, and 744 tokens had been issued to winning wallets. Chia network required the re-issue of all CATs running on CAT1 and update to CAT2 in order to rectify this. Once our token was re-issued to run using the CAT2 standard, 744 tokens were sent from the initial mint to the following wallet; xch1rnk9tcy7u0q9hxwnzad3y2x0x4ssxm9q8ek2xjp4ch7vxr6accfqw3nd47. These tokens were then distributed to all CAT1 owners to replace their now defunct CAT1 tokens, in line with instructions from Chia company. 



